import sys
from json import dumps
from httplib2 import Http

build_by = sys.argv[1]
build_status= sys.argv[2]
job_name= sys.argv[3]
Message= sys.argv[4]
display_message=None
failed="<font color=\"#ff0000\">"
success="<font color=\"#00FF00\">"
buildno= sys.argv[5]
build_url= sys.argv[6]
googlechatnotification_url=sys.argv[7]
if (build_status=="Failed"):
    status_color=failed
    display_message="<b>MESSAGE: </b>"+Message
else:
  status_color=success
def main():    
    """Hangouts Chat incoming webhook quickstart."""
    bot_message = {
        'text' : 'Hello from a Python script!'}
    message_headers = {'Content-Type': 'application/json; charset=UTF-8'}
    http_obj = Http()
    y = {
        "cards": [
            {
              "header": {
                "title": "<b>JOB NAME: </b>"+job_name, #jobname+ build number   #information should be in caps and bold
                "subtitle": "BUILD NO: "+buildno, #job status
                "imageUrl": "https://www.eficode.com/hubfs/images/blogs/Imported_Blog_Media/cool-jenkins.png"#"https://cdn-media-1.freecodecamp.org/images/1*RmcSmwPhUn8ljLiiwYxK0A.png"
              },
              "sections":[
                {
                    "widgets":[
                      {
                        "keyValue" : {
                          "topLabel" :"<b>BUILD BY</b>",
                          "content" : "<b>"+build_by+"</b>",
                          "bottomLabel" : "<b>Click for more details</b>",
                          "onClick":{"openLink":{"url":build_url}},
                          "button": 
                            {
                                    "textButton": {
                                       "text": status_color+str(build_status).capitalize()+"</font>,<b>Click for more details</br>",
                                       "onClick": {
                                           "openLink": {
                                                "url": build_url
                                            }
                                        }
                                      }
                                  }
                        }
                        }
                    ]
              },{
                "widgets" :[{
                  "textParagraph":{
                        "text":display_message
                      }
                }]
              }
              ]
            }
          ]
    }
    response = http_obj.request(
        uri=googlechatnotification_url,
        method='POST',
        headers=message_headers,
        body=dumps(y),
    )
    print(response)
if __name__ == '__main__':
    main()
